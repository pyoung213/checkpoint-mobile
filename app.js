var checkpointApp = angular.module('checkpointApp', ['ngRoute','ngSanitize', 'ngAnimate']);

checkpointApp.config(function ($routeProvider, $locationProvider){
    $routeProvider.
        when('/search', {
            templateUrl: 'search/search.html',
            controller: 'SearchCtrl'
        }).
        when('/toc', {
            templateUrl: 'toc/toc.html',
            controller: 'TocCtrl'
        }).
        when('/news', {
            templateUrl: 'news/news.html',
            controller: 'NewsCtrl'
        }).
        when('/tools/:type?/:something?', {
            templateUrl: 'tools/tools.html',
            controller: 'ToolsCtrl'
        }).
        when('/community', {
            templateUrl: 'community/community.html',
            controller: 'CommunityCtrl'
        }).
        when('/document/:docId', {
            templateUrl: 'document/document.html',
            controller: 'DocumentCtrl'
        }).
        when('/notes', {
            templateUrl: 'notesSearch/notesSearch.html',
            controller: "NotesSearchCtrl"
        }).
        otherwise({
            redirectTo: '/search'
        });

        // $locationProvider.html5Mode(true);
});

checkpointApp.controller('AppCtrl', function ($scope, $location, NotesPersist) {
    $scope.go = function (path) {
    	$location.path(path);
    }

    $scope.notesPersist = NotesPersist;

    var docNote = {};
    docNote.docName = "Ann ¶ 5015.07(15). Controlling animal breeding";
    docNote.text = "This is an awesome note I just wrote!";
    docNote.docId = "doc1";
    docNote.time = "1395522315882";
    $scope.notesPersist.push(docNote);
    docNote = {};
    docNote.docName = "¶D-4183. Prevention of cruelty to children or animals.";
    docNote.text = "This is another Angular spanking awesome note!";
    docNote.docId = "doc2";
    docNote.time = "1395522317801";
    $scope.notesPersist.push(docNote);
    docNote = {};
    docNote.docName = "Ann ¶ 5015.07(15). Controlling animal breeding";
    docNote.text = "I love coding for notes now!";
    docNote.docId = "doc1";
    docNote.time = "1395522330656";
    $scope.notesPersist.push(docNote);
    docNote = {};
    docNote.docName = "PLR 5411228820A -- IRC Sec(s). 6418, 11/22/1954";
    docNote.text = "Probably need a note Id or something";
    docNote.docId = "doc3";
    $scope.notesPersist.push(docNote);
    docNote.time = "1395522360399";
    docNote = {};
});

checkpointApp.factory("User",function(){
        return {};
});

checkpointApp.factory("ToolsData",function(){
        return {};
});

checkpointApp.factory("NotesPersist",function(){
        return [];
});
