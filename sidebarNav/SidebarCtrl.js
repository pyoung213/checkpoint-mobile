checkpointApp.controller('SidebarCtrl', function ($scope, $location, dataFactory) {
    
    dataFactory.getData('sidebar').success(function(data) {
			$scope.sidebarData = data;
	});

    $scope.isActive = function(route) {
        return ($location.path().indexOf(route) === 0);
    };


    $scope.isShowNavSlideOut = false;

    $scope.showNavSlideOut = function (navData){
        $scope.isShowNavSlideOut = !$scope.isShowNavSlideOut;
        $scope.navSlideOutData = navData.slideOut.data;
    }
});