checkpointApp.controller('DocumentCtrl', function ($scope, dataFactory, $routeParams, NotesPersist, $sce) {
	$scope.notesPersist = NotesPersist;
	rangy.init();

	dataFactory.getDocumentData('document').success(function(data) {
			var docId = $routeParams.docId;
			$scope.documentData = data[docId];
			var html = $scope.documentData.body;
			$scope.documentBodyData = $sce.trustAsHtml(html);
	});

	$scope.addNote = function (note) {
		var docNote = {};
		docNote.docName = $scope.documentData.title;
		docNote.text = note;
		docNote.docId = $scope.documentData.docId;
		docNote.time = new Date().getTime();
		$scope.notesPersist.unshift(docNote);
		$scope.noteText = '';
	}

	$scope.changeNoteText = function () {
		this.showEditNote = false;
	}

	$scope.removeNote = function (note) {
		$scope.notesPersist = _.pull($scope.notesPersist,note);
	}

	$scope.showDocNote = function () {
	}

	
	// $scope.cssApplier = function () {
	// 	var sel = rangy.createCssClassApplier("highlight-doc", {normalize: true});
	// 	sel.toggleSelection();
	// }
});