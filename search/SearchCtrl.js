checkpointApp.controller('SearchCtrl', function ($scope, $http, User) {
	$scope.user = User;

	if($scope.user.sourcesFilter == null) {
		$scope.user.sourcesFilter = [];
	}
	if($scope.user.searchView == null) {
    	$scope.user.searchView = "search/search-home.html";
	}
    $scope.switchView = function() {
    		$scope.user.searchView = "search/search-results.html";
    };

	$http.get('checkpoint-data/search/search-results.json').success(function(data) {      
	        $scope.searchResults = data;
	});
	if($scope.user.sources == null) {
		$http.get('checkpoint-data/sources/sources.json').success(function(data) {      
	    	    $scope.user.sources = data;
		});
	}

	$scope.toggleFilter = function () {
        if($scope.filterSlideOut == null) {
            $scope.filterSlideOut = 'true';
        } else {
            $scope.filterSlideOut = !$scope.filterSlideOut;
        }     
    }

    $scope.removeFilter = function () {
        _this = this;
        if(_this.source.sourceId) {
            angular.forEach($scope.user.sourcesFilter, function(value, key) {
                if(angular.equals(value.sourceId, _this.source.sourceId)) {
                    $scope.user.sourcesFilter.splice(key,1);
                }
            });

            angular.forEach($scope.user.sources, function(value, key) {
                if(angular.equals(value.sourceId, _this.source.sourceId)) {
                    value.sourceSelected = false;
                }
            }); 
        } else {
            angular.forEach($scope.user.sourcesFilter, function(value, key) {
                if(angular.equals(value.name, _this.source.name)) {
                    $scope.user.sourcesFilter.splice(key,1);
                }
            });
        }
        
    }
});