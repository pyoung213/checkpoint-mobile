checkpointApp.controller('NotesSearchCtrl', function ($scope, NotesPersist) {
	$scope.notesPersist = NotesPersist;

	$scope.predicate = 'time';
	$scope.reverse = false;

	$scope.removeNote = function (note) {
		$scope.notesPersist = _.pull($scope.notesPersist,note);
	}

	$scope.changeNoteText = function (note) {
		this.showEditNote = false;
	}
});