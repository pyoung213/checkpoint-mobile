checkpointApp.controller('ToolsCtrl', function ($scope, dataFactory, $routeParams, $location) {
	$scope.breadcrumb = [];
	$scope.breadcrumb.push({'name':'Tools','href':'tools'});
	
	if(!angular.isUndefined($routeParams.type)) {
		$scope.breadcrumb.push({'name':'Create-a-Chart','href':'createachart'});
	}

	dataFactory.getData('tools').success(function(data) {
		if(angular.isUndefined($routeParams.type)) {
			$scope.toolsData = data.tools;
		} else {
			$scope.toolsData = data[$routeParams.type];
		}
	});

	$scope.changeData = function () {
		var href = this.href;
		$scope.toolsData = this.tool.type;
	}
});