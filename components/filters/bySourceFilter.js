checkpointApp.filter('bySource', function() {
    return function(resultsList,filterList) {
        if(filterList.length) {
            var items = {
                filterList: filterList,
                out:  []
            };
            
            angular.forEach(resultsList, function (result, key) {
                for(var i = 0;i < filterList.length; i++) {
                    //if true then keyword
                    if(angular.equals(this.filterList[i].sourceId, result.ofsid)) {
                        items.out.push(result);
                        i = filterList.length;
                    }
                }
                         
            } , items);

            return items.out;

        } else {     
            return resultsList;  
        };
    }   
});