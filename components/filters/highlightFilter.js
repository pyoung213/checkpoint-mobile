checkpointApp.filter('highlight', function () {
  return function (text, search, caseSensitive) {
    if (search || angular.isNumber(search)) {
      text = text.toString();
      search = search.toString();
      return text.replace(new RegExp(search, 'gi'), '<span class="highlight-text">$&</span>');
    } else {
      return text;
    }
  };
});