checkpointApp.directive('navSlideOut', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: "=",
      ctrl: "=",
      show: "=",
      side: "@"
    },
    link: function(scope, element, attrs) {
      
      scope.closeWindow = function() {
        scope.show = false;
      };
    },
    templateUrl: "components/directives/navSlideOut.html"
  };
});