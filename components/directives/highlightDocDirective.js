checkpointApp.directive('highlightdoc', function () {
    return function (scope, element, attrs) {
        
    };
});


checkpointApp.directive('highlightdoc', function() {
  return {
    restrict: 'A',
    scope: {
        doc:"="
    },
    transclude: true,
    link: function(scope, element, attrs) {
        scope.cssApplier = function () {
            var sel = rangy.createCssClassApplier("highlight-doc", {normalize: true});
            sel.toggleSelection();
        }
    },
    templateUrl: "components/directives/highlightDoc.html"
  }
});