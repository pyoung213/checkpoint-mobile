checkpointApp.directive('slideOutWindow', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: "=",
      show: "=",
      filter: "=",
      side: "@"
    },
    link: function(scope, element, attrs) {
      
      scope.closeWindow = function() {
        scope.show = false;
      };

      scope.sourceSelect = function() {
        var _this = this;
        this.source.sourceSelected = !this.source.sourceSelected;
        if(this.source.sourceSelected) {
          scope.filter.push(this.source);
        } else {
          angular.forEach(scope.filter, function(value, key) {
            if(angular.equals(_this.source.sourceId, value.sourceId)) {
              scope.filter.splice(key,1);
            }
          });         
        }
      };

      scope.addKeywordFilter = function (keyword) {
        if(!angular.isUndefined(keyword)) {
          var isExists = false;
          angular.forEach(scope.filter, function(value, key) {
            if(angular.equals(value.name, keyword)) {
              alert('Keyword already exists');
              isExists = true;
            };
          })
          if (!isExists) {
            scope.filter.push({name:keyword});
            scope.keywordFilter = '';
          }   
        };
      }
    },
    templateUrl: "components/directives/slideOutWindow.html"
  };
});