checkpointApp.directive('breadcrumb', function() {
  return {
    restrict: 'E',
    scope: {
      data: "="
    },
    replace: true,
    link: function(scope, element, attrs) {
    },
    templateUrl: "components/directives/breadcrumbDirective.html"
  }
});