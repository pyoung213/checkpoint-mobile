checkpointApp.factory('dataFactory', ['$http', function($http) {

    var urlBase = 'checkpoint-data/';
    var dataFactory = {};

    dataFactory.getData = function (id) {
        return $http.get(urlBase + '/' + id + '/' + id + '.json');
    };

    dataFactory.getDocumentData = function (id) {
        return $http.get(urlBase + '/' + id + '/' + id + '.json');
    };

    return dataFactory;
}]);